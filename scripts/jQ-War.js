$(function() {
	var deck; // Holds the 52 cards of the deck
	var P_hand; // Cards in player's hand
	var S_hand; // Cards in Slick's hand
	var player_cards; // player's cards played
	var slick_cards; // Slick's cards played
	var name = ""; // player's name

	// Set up buttons for initial page load
	$('#newPlayer').on('click', PromptName);
	$('#deal').on('click', DealNew);

	// Prompts for player name and deals a new game
	function PromptName() {
		do {
			name = prompt("Please enter your name");
		} while (name == "" || name == null)
		$('.pName').html(name);
		DealNew();
	}

	// Deals a new game. Prompts for player name if name has not already been set.
	function DealNew() {
		if (name == "") {
			PromptName();
		}
		else {
			LoadVars(); // Reset game variables
			$('img').remove(); // Remove any existing images from previous game
			$('#win').html('');

			for (var i = 52; i > 0; i--) {
				indexToDeal = Math.floor(Math.random() * deck.length); // Get random from remaining cards
				if (i % 2 == 0) { // Give card to Slick
					S_hand.cards.push(deck[indexToDeal]);
				}
				else { // Give card to player
					P_hand.cards.push(deck[indexToDeal]);
				}
				deck.splice(indexToDeal, 1); // Remove card from deck
			}

			UpdateStats(); // Update card count and scores

			$('#newPlayer').off('click', PromptName);
			$('#deal').off('click', DealNew);
			$('#play').on('click', PlayCard);
		}
	}

	function PlayCard() {
		// Get last card for both players
		pCard = P_hand.cards[P_hand.cards.length - 1];
		cCard = S_hand.cards[S_hand.cards.length - 1];

		// Display cards
		if (P_hand.cards.length == 26) { // First move - append image
			$('#pHand').append('<img class="pImg" src="images/' + pCard.suit + '/' + pCard.name + '.jpg" />');
			$('#cHand').append('<img class="sImg" src="images/' + cCard.suit + '/' + cCard.name + '.jpg" />');
		}
		else { // Already have card - just replace image source
			$('#pHand').children('img').attr('src', 'images/' + pCard.suit + '/' + pCard.name + '.jpg');
			$('#cHand').children('img').attr('src', 'images/' + cCard.suit + '/' + cCard.name + '.jpg');
		}
		
		// Get round winner and add cards to winner
		if (pCard.trump > cCard.trump) {
			player_cards.push(pCard);
			player_cards.push(cCard);
		}
		else {
			slick_cards.push(pCard);
			slick_cards.push(cCard);
		}

		// Remove last card from player decks
		P_hand.cards.pop();
		S_hand.cards.pop();

		UpdateStats();
	}

	function UpdateStats() {
		var pScore = 0;
		var cScore = 0;

		$('#cardCount').html(P_hand.cards.length);

		for (i = 0; i < player_cards.length; i++) {
			pScore += player_cards[i].value;
		}
		for (i = 0; i < slick_cards.length; i++) {
			cScore += slick_cards[i].value;
		}
		$('#pScore').html(pScore);
		$('#cScore').html(cScore);

		if (P_hand.cards.length == 0) { // Game is over
			setTimeout(function() {
				var chkImg = 'images/other/check.png';
			let xImg = 'images/other/x.png';
			if (pScore > cScore) {
				$('#pHand').children('img').attr('src', chkImg);
				$('#cHand').children('img').attr('src', xImg);
				$('#win').html(name + ' is the winner!');
			}
			else if (cScore > pScore) {
				$('#pHand').children('img').attr('src', xImg);
				$('#cHand').children('img').attr('src', chkImg);
				$('#win').html('Slick is the winner!');
			}
			else {
				$('#pHand').children('img').attr('src', xImg);
				$('#cHand').children('img').attr('src', xImg);
				$('#win').html(name + ' and Slick tied!');
			}

			$('#newPlayer').on('click', PromptName);
			$('#deal').on('click', DealNew);
			$('#play').off('click', PlayCard);
			}, 1000);
		}
	}

	function LoadVars() {
		player_cards = new Array();
		slick_cards = new Array();
		P_hand = {
			cards: new Array()
		}
		S_hand = { // Unplayed cards in Slick's hand
			cards: new Array()
		}
		deck = [
			new card('Ace', 'Hearts', 11, 142),
			new card('Two', 'Hearts', 2, 22),
			new card('Three', 'Hearts', 3, 32),
			new card('Four', 'Hearts', 4, 42),
			new card('Five', 'Hearts', 5, 52),
			new card('Six', 'Hearts', 6, 62),
			new card('Seven', 'Hearts', 7, 72),
			new card('Eight', 'Hearts', 8, 82),
			new card('Nine', 'Hearts', 9, 92),
			new card('Ten', 'Hearts', 10, 102),
			new card('Jack', 'Hearts', 10, 112),
			new card('Queen', 'Hearts', 10, 122),
			new card('King', 'Hearts', 10, 132),
			new card('Ace', 'Diamonds', 11, 141),
			new card('Two', 'Diamonds', 2, 21),
			new card('Three', 'Diamonds', 3, 31),
			new card('Four', 'Diamonds', 4, 41),
			new card('Five', 'Diamonds', 5, 51),
			new card('Six', 'Diamonds', 6, 61),
			new card('Seven', 'Diamonds', 7, 71),
			new card('Eight', 'Diamonds', 8, 81),
			new card('Nine', 'Diamonds', 9, 91),
			new card('Ten', 'Diamonds', 10, 101),
			new card('Jack', 'Diamonds', 10, 111),
			new card('Queen', 'Diamonds', 10, 121),
			new card('King', 'Diamonds', 10, 131),
			new card('Ace', 'Clubs', 11, 143),
			new card('Two', 'Clubs', 2, 23),
			new card('Three', 'Clubs', 3, 33),
			new card('Four', 'Clubs', 4, 43),
			new card('Five', 'Clubs', 5, 53),
			new card('Six', 'Clubs', 6, 63),
			new card('Seven', 'Clubs', 7, 73),
			new card('Eight', 'Clubs', 8, 83),
			new card('Nine', 'Clubs', 9, 93),
			new card('Ten', 'Clubs', 10, 103),
			new card('Jack', 'Clubs', 10, 113),
			new card('Queen', 'Clubs', 10, 123),
			new card('King', 'Clubs', 10, 133),
			new card('Ace', 'Spades', 11, 144),
			new card('Two', 'Spades', 2, 24),
			new card('Three', 'Spades', 3, 34),
			new card('Four', 'Spades', 4, 44),
			new card('Five', 'Spades', 5, 54),
			new card('Six', 'Spades', 6, 64),
			new card('Seven', 'Spades', 7, 74),
			new card('Eight', 'Spades', 8, 84),
			new card('Nine', 'Spades', 9, 94),
			new card('Ten', 'Spades', 10, 104),
			new card('Jack', 'Spades', 10, 114),
			new card('Queen', 'Spades', 10, 124),
			new card('King', 'Spades', 10, 134)
		];
	}

	function card(name, suit, value, trump) {
		this.name = name;
		this.suit = suit;
		this.value = value;
		this.trump = trump;
	}
});